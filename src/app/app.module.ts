import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AvengersFormComponent } from './avengers-form/avengers-form.component';
import { AvengerListComponent } from './avengers-form/avenger-list/avenger-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AvengersFormComponent,
    AvengerListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
