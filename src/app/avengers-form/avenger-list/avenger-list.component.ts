import { Avenger } from './../models/avenger';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-avenger-list',
  templateUrl: './avenger-list.component.html',
  styleUrls: ['./avenger-list.component.scss']
})
export class AvengerListComponent implements OnInit {
@Input() public avengerCompleted: Avenger | null = null;
public avengerList: Array<Avenger | null> = [];

  constructor() { }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    this.newAvenger();
  }

  public newAvenger(){
    this.avengerCompleted != undefined || this.avengerCompleted != null ?
    this.avengerList.push(this.avengerCompleted): console.log('vacio');
  }

}
