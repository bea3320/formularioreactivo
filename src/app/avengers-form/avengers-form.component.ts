import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Avenger } from './models/avenger';

@Component({
  selector: 'app-avengers-form',
  templateUrl: './avengers-form.component.html',
  styleUrls: ['./avengers-form.component.scss']
})
export class AvengersFormComponent implements OnInit {

  public avengerForm: FormGroup;
  public submitted: boolean = false;
  public avengerCompleted: Avenger | null = null;

  constructor(private formBuilder: FormBuilder) {
    this.avengerForm = this.formBuilder.group({
      superName: ['', [Validators.required, Validators.minLength(1)]],
      alias: ['', [Validators.maxLength(50)]],
      age: ['', [Validators.required, Validators.min(18)]],
      email: ['', [Validators.email, Validators.required]],
    });
  }

  ngOnInit(): void { /*empty*/}

  public onSubmit(): void{
    this.submitted = true;

    if(this.avengerForm?.valid){
      const AVENGER: Avenger = {
        superName: this.avengerForm.get('supername')?.value,
        alias: this.avengerForm.get('alias')?.value,
        age: this.avengerForm.get('age')?.value,
        email: this.avengerForm.get('email')?.value,
      };
      this.avengerCompleted = AVENGER;
      this.avengerForm.reset();
      this.submitted = false;
    }
  }
}
