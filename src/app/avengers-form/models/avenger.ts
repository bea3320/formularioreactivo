export interface Avenger {
  superName: string;
  alias: string;
  age: number;
  email: string;
}
